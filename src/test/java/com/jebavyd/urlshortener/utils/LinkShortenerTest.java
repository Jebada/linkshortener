package com.jebavyd.urlshortener.utils;

import com.jebavyd.urlshortener.repository.LinkRepository;
import com.jebavyd.urlshortener.repository.model.LinkEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class LinkShortenerTest {

    private LinkRepository linkRepository = mock(LinkRepository.class);
    private LinkShortener linkShortener = new LinkShortener("https://test-base-url.com/", linkRepository);

    @Before
    public void setup() {
        when(linkRepository.save(any()))
                .thenAnswer(( Answer<Mono<LinkEntity>>) invocationOnMock ->
                Mono.just((LinkEntity) invocationOnMock.getArguments()[0]));
    }

    @Test
    public void shortenLinks() {
        StepVerifier.create(linkShortener.shortenLink("https://spring.io"))
                    .expectNextMatches(result -> result != null
                            && result.length() > 0
                            && result.startsWith("https://test-base-url.com/"))
                    .expectComplete()
                    .verify();
    }

    @Test
    public void addsHttpsToLink() {
        StepVerifier.create(
                    linkShortener.shortenLink("spring.io"))
                .expectNextMatches(
                        result ->
                                result !=null
                                        && result.startsWith("https://"))
                .expectComplete()
                .verify();
    }
}