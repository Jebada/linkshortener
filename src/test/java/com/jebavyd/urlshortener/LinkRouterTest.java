package com.jebavyd.urlshortener;

import com.jebavyd.urlshortener.handlers.PostLinkHandler;
import com.jebavyd.urlshortener.repository.RedisLinkRepository;
import com.jebavyd.urlshortener.repository.model.LinkEntity;
import com.jebavyd.urlshortener.routers.LinkRouter;
import com.jebavyd.urlshortener.utils.LinkShortener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {LinkRouter.class, PostLinkHandler.class})
@WebFluxTest
public class LinkRouterTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private LinkShortener linkShortener;

   // @MockBean
  //  private RedisLinkRepository redisLinkRepository;

    @Test
    public void shortensLink() {
        when(linkShortener.shortenLink("https://spring.io")).thenReturn(Mono.just("http://localhost:8080/2021aaa"));
        webTestClient.post()
                .uri("/link")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue("{\"link\":\"https://spring.io\"}")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$.shortenedLink")
                .value(val -> assertThat(val).isEqualTo("http://localhost:8080/2021aaa"));
    }

    @Test
    public void redirectsToOriginalLink() {
        //when(redisLinkRepository.findByKey("aaa21123"))
         //       .thenReturn(
         //               Mono.just(
          //                      new LinkEntity("https://spring.io", "aaa21123")));

        webTestClient.get()
                .uri("/aaa21123")
                .exchange()
                .expectStatus()
                .isPermanentRedirect()
                .expectHeader()
                .value("Location", location -> assertThat(location).isEqualTo("https://spring.io"));
    }



}
