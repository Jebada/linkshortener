package com.jebavyd.urlshortener.repository;

import com.jebavyd.urlshortener.repository.model.LinkEntity;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public class        RedisLinkRepository implements LinkRepository {

    private final ReactiveRedisOperations<String, String> operations;

    public RedisLinkRepository(ReactiveRedisOperations<String, String> operations) {
        this.operations = operations;
    }

    @Override
    public Mono<LinkEntity> save(LinkEntity linkEntity) {
        return operations.opsForValue()
                         .set(linkEntity.getKey(), linkEntity.getOriginalLink())
                         .map(__ -> linkEntity);
    }

    @Override
    public Mono<LinkEntity> findByKey(String key) {
        return operations.opsForValue()
                         .get(key)
                         .map(result -> new LinkEntity(result, key));
    }
}
