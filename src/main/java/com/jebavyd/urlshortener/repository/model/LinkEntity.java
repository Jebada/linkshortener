package com.jebavyd.urlshortener.repository.model;


import lombok.Value;

@Value
public class LinkEntity {
    String originalLink, key;
}
