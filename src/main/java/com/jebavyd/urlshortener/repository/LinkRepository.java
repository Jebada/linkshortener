package com.jebavyd.urlshortener.repository;

import com.jebavyd.urlshortener.repository.model.LinkEntity;
import reactor.core.publisher.Mono;

public interface LinkRepository {
    Mono<LinkEntity> save(LinkEntity linkEntity);

    Mono<LinkEntity> findByKey(String key);
}
