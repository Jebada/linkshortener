package com.jebavyd.urlshortener.handlers;

import com.jebavyd.urlshortener.models.LinkShortenerRequest;
import com.jebavyd.urlshortener.models.LinkShortenerResponse;
import com.jebavyd.urlshortener.utils.LinkShortener;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;


@Component
@RequiredArgsConstructor
@Slf4j
public class PostLinkHandler {

   private final LinkShortener linkShortener;

    /**
     * Take server request and shorten it with LinkShortener component
     * @param request Server request
     * @return Server response containing shortened link at <i>shortenedLink</i> JSON path.
     */
   public Mono<ServerResponse> handle(ServerRequest request) {
       // TODO validate that link is not empty!
       return request.bodyToMono(LinkShortenerRequest.class) // Convert request to Mono<LinkRequest>
               .flatMap( linkObject -> linkShortener.shortenLink(linkObject.getLink()))
               .flatMap( randomKey -> ServerResponse.ok()   // return JSON response
                               .contentType(MediaType.APPLICATION_JSON)
                               .body(fromValue(new LinkShortenerResponse(randomKey))))
               .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST, "No link was provided")));
   }


}
