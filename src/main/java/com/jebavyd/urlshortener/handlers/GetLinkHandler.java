package com.jebavyd.urlshortener.handlers;

import com.jebavyd.urlshortener.repository.LinkRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.web.reactive.function.server.ServerResponse.permanentRedirect;
import static org.springframework.web.reactive.function.server.ServerResponse.temporaryRedirect;

@Component
@RequiredArgsConstructor
@Slf4j
public class GetLinkHandler {

    private final LinkRepository linkRepository;


    public Mono<ServerResponse> handle(ServerRequest request) {
        String key = request.pathVariable("key");

        return linkRepository.findByKey(key)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Key not found")))
                .doOnNext(linkEntity -> log.info("URL for key {} found: {}", linkEntity.getKey(), linkEntity.getOriginalLink()))
                    .flatMap(linkEntity ->
                            temporaryRedirect(
                                    URI.create(linkEntity.getOriginalLink()))
                                    .location(URI.create(linkEntity.getOriginalLink()))
                            .build());

    }
}
