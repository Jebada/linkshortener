package com.jebavyd.urlshortener.utils;

import com.jebavyd.urlshortener.repository.LinkRepository;
import com.jebavyd.urlshortener.repository.model.LinkEntity;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class LinkShortener {

    private final String baseUrl;
    private final LinkRepository linkRepository;

    public LinkShortener(@Value("${app.baseUrl}") String baseUrl, LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
        this.baseUrl = baseUrl;
    }

    public Mono<String> shortenLink(String link) {
        String randomKey = RandomStringUtils.randomAlphabetic(6); // Generate key for link
        String linkToSave = startWithHttps(link) ? link : addHttpsToUrl(link);// normalize link
        // Save to DB and return shortened link
        return linkRepository.save(new LinkEntity(linkToSave, randomKey))
                .map(result -> baseUrl + result.getKey());
    }

    private String addHttpsToUrl(String url) {
        return ("https://" + url);
    }

    private boolean startWithHttps(String url) {
        return url.startsWith("http://");
    }
}
