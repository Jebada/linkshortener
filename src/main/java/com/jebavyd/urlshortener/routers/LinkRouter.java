package com.jebavyd.urlshortener.routers;

import com.jebavyd.urlshortener.handlers.GetLinkHandler;
import com.jebavyd.urlshortener.handlers.PostLinkHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.accept;


@Configuration
public class LinkRouter {

    @Bean
    public RouterFunction<ServerResponse> route(PostLinkHandler postLinkHandler,
                                                GetLinkHandler getLinkHandler) {
        return RouterFunctions.route()
                .POST("/link", accept(MediaType.APPLICATION_JSON), postLinkHandler::handle)
                .GET("/{key}", getLinkHandler::handle)
                .build();
    }
}
